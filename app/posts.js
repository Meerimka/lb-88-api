const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');

const Post = require('../models/Post');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', (req, res) => {
        Post.find().populate('username').sort({"dateTime": -1})
            .then(post => {
                res.send(post)
            })
            .catch(() => res.sendStatus(500));
    });

router.get('/:id', (req, res) => {
    Post.findById(req.params.id).populate('username')
        .then(post => {
            res.send(post);
        })
        .catch(() => res.sendStatus(500));
});

router.post('/', [auth, upload.single('image')], async (req, res) => {

    const postData = {
        title: req.body.title,
        description: req.body.description,
        dateTime: new Date().toISOString(),
        username: req.user._id
    };

    if (req.file) {
        postData.image = req.file.filename;
    }

    const post = new Post(postData);

    post.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

module.exports = router;
