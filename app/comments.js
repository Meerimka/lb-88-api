const express = require('express');
const Comment = require('../models/Comments');
const auth = require('../middleware/auth');



    const router = express.Router();

    router.get('/:id',auth, (req, res) => {
        Comment.find({postId: req.params.id}).populate('username')
            .then(comment => {
                res.send(comment)
            })
            .catch(() => res.sendStatus(500));

    });


    router.post('/',auth,(req, res) => {
        const comData = new Comment({
            comment: req.body.comment,
            postId: req.body.postId,
            username: req.user._id
        });

        comData.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });






module.exports = router;
