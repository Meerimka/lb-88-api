const mongoose = require('mongoose');
const config = require('./config');

const Post = require('./models/Post');
const User = require('./models/User');
const Comment = require('./models/Comments');

const run =async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for(let collection of collections){
        await collection.drop();
    }

    const [user1, user2] = await User.create(
        {username: 'John',
            password: '123',
            token: '125425'
        },
        {username: 'Jack',
            password: '123',
            token: '125425524'
        }
    );

    const [post1, post2] = await Post.create(
        {
            title: "some title",
            image: "eminem.jpg",
            username: user1._id,
            description: "Marshall Bruce Mathers III (born October 17, 1972), known professionally as Eminem",
            dateTime: 2015
        },
        {
            title: "some title 2",
            image: "logic.jpg",
            username: user2._id,
            description: "Some description",
            dateTime: 2015
        }
    );
    const [comment1,comment2] = await Comment.create(
        {
            comment: "something",
            username: user1._id,
            postId: post1._id
        },
    {
        comment: "something",
        username: user2._id,
        postId: post1._id
    }
    )
};

run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});