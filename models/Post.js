const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema ({
   title:{
    type: String,
    required: true
   },
    username:{
       type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    description:{
       type: String
   },
   image:{
       type:String
   },
    dateTime:{
        type: String ,
        unique: true
    }
});

const Post = mongoose.model('Post',PostSchema);

module.exports = Post;